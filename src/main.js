import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router';

import { OneCardLeft, GameSetup, GameRound, RoundScores, GameOver } from './components';
import { MIN_PLAYERS } from './constants';
import store from './store';

function ensureSetup (route, replace) {
    const state = store.getState();

    if (state.players.length < MIN_PLAYERS) {
        console.log('Not enough players');

        replace({
            pathname: '/setup',
            state: { nextPathname: route.location.pathname }
        });
    }
}

function ensureRoundOver (route, replace) {
}

function ensureGameOver (route, replace) {
}

render(
    <Provider store={store}>
        <Router>
            <Route path="/" component={OneCardLeft} />
            <Route path="/setup" component={GameSetup} />
            <Route path="/round" component={GameRound} onEnter={ensureSetup} />
            <Route path="/scores" component={RoundScores} onEnter={ensureRoundOver} />
            <Route path="/winner" component={GameOver} onEnter={ensureGameOver} />
        </Router>
    </Provider>,
    document.getElementById('main')
);


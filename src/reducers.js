
'use strict';

import { combineReducers } from 'redux';

function goal (state = 500, action) {
    switch (action.type) {
        case 'GOAL_SET':
            return action.value;

        default:
            return state;
    }
}

function players (state = [], action) {
    switch (action.type) {
        case 'PLAYER_ADD':
            return [
                ...state,
                {
                    name: action.name,
                    scores: []
                }
            ];

        case 'PLAYER_REMOVE':
            return [
                ...state.slice(0, action.index),
                ...state.slice(action.index + 1)
            ];

        default:
            return state;
    }
}

export default combineReducers({
    players,
    goal
});


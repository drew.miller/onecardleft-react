
export function addPlayer (name) {
    return {
        type: 'PLAYER_ADD',
        name: name
    };
};

export function removePlayer (index) {
    return {
        type: 'PLAYER_REMOVE',
        index: index
    };
};

export function setGoal (goal) {
    return {
        type: 'GOAL_SET',
        value: goal
    };
};


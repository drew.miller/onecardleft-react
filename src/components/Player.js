import React, { Component } from 'react';

export default class Player extends Component {
    render () {
        return (
            <li className="player">
                <h2>{this.props.name}</h2>
            </li>
        );
    }
};


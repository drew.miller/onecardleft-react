import React, { Component } from 'react';
import { Link } from 'react-router';

export default class OneCardLeft extends Component {
    render () {
        return (
            <div>
                <h1>One Card Left</h1>
                <p>Keep track of your games in that card game. <strong>You Know</strong> the one.</p>
                <Link to="/setup">Start</Link>
            </div>
        );
    }
};


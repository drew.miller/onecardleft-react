import React, { createClass } from 'react';
import Player from './Player';

const PlayerList = createClass({
    enoughPlayers: function (players, required) {
        return players.length < required;
    },

    handleRemovePlayer: function (index) {
        this.props.removePlayer(index);
    },

    render: function () {
        const { dispatch, players, requiredPlayers } = this.props;

        const playerList = players.map((player, index) => {
            return (
                <li key={index}>
                    {player.name}
                    <button onClick={() => this.handleRemovePlayer(index)}>Remove</button>
                </li>
            );
        });

        return (
            <div>
                { this.enoughPlayers(players, requiredPlayers) ? (
                    <p>You need more players sir</p>
                ) : null }
                <ul className="player-list">{playerList}</ul>
            </div>
        );
    }
});

export default PlayerList;


import React, { createClass } from 'react';
import { GOALS } from '../constants';

const GoalSelect = createClass({
    handleGoalChange: function (e) {
        this.props.setGoal(parseInt(e.target.value, 10));
    },

    render: function () {
        const currentGoal = this.props.goal;
        const goalOptions = GOALS.map((value, index) => {
            return (
                <option key={index} value={value}>{value}</option>
            );
        });

        return (
            <select value={currentGoal} onChange={this.handleGoalChange}>{goalOptions}</select>
        );
    }
});

export default GoalSelect;


import React, { createClass } from 'react';
import { Link } from 'react-router';

const GameRound = createClass({
    roundOver: function () {
    },

    render: function () {
        const { dispatch, players } = this.props;

        return (
            <div>
                <h1>TBC</h1>
                <RoundWinner players={players} />
                <button onClick={roundOver}>Round Over</button>
            </div>
        );
    }
});

export default GameRound;


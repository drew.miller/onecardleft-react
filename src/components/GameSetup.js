import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { addPlayer, removePlayer, setGoal } from '../actions';
import PlayerEntry from './PlayerEntry';
import PlayerList from './PlayerList';
import GoalSelect from './GoalSelect';

class GameSetup extends Component {
    render () {
        const { dispatch, players, goal } = this.props;

        return (
            <div>
                <h1>Setup Game</h1>
                <h2>Set Goal</h2>
                <GoalSelect goal={goal} setGoal={goal => dispatch(setGoal(goal))} />
                <h2>Enter Players</h2>
                <PlayerEntry addPlayer={name => dispatch(addPlayer(name))} />
                <PlayerList requiredPlayers="3" removePlayer={index => dispatch(removePlayer(index))} players={players} />
                <Link to="/round">Start</Link>
            </div>
        );
    }
};

function select (state) {
    return {
        players: state.players,
        goal: state.goal
    };
}

export default connect(select)(GameSetup);


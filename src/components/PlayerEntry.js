import React, { createClass } from 'react';

const PlayerEntry = createClass({
    getInitialState: function () {
        return {
            name: ''
        };
    },

    handleNameChange: function (e) {
        this.setState({
            name: e.target.value
        });
    },

    handleAddPlayer: function (e) {
        e.preventDefault();

        const name = this.state.name.trim();

        // send to redux store
        this.props.addPlayer(name);

        this.setState({
            name: ''
        });
    },

    render: function () {
        return (
            <div>
                <input type="text" placeholder="Name" name="name" value={this.state.name} onChange={this.handleNameChange} />
                <button id="add" onClick={this.handleAddPlayer}>Add</button>
            </div>
        );
    }
});

export default PlayerEntry;

